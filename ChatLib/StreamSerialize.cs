﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ChatLib
{
    public static class StreamSerialize
    {
        public static void Serialize(Stream streamInput, Message message)
        {
            try
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(streamInput, message);
            }
            catch (IOException)
            {

            }
        }

        public static Message Deserialize(Stream streamInput)
        {
            try
            {
                var formatter = new BinaryFormatter();
                return (Message)formatter.Deserialize(streamInput);
            }
            catch (SerializationException)
            {
                return Message.GerErrorCodeObject(502);
            }
            catch (IOException)
            {
                return Message.GerErrorCodeObject(503);
            }
        }

    }
}
