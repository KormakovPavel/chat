﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace ChatLib
{
    [Serializable]
    public class Message : ISerializable
    {
        private int statusCode;
        private static string userName;
        private string text;
        public int StatusCode
        {
            set { statusCode = value; }
            get { return statusCode; }
        }

        public string UserName
        {
            set { userName = value; }
            get { return userName; }
        } 
        public string Text
        {
            set { text = value; }
            get { return text; }
        }
        public Message(int _statusCode, string _userName, string _text)
        {
            statusCode = _statusCode;
            userName = _userName;
            text = _text;
        }

        public Message(string _userName, string _text)
        {
            statusCode = 200;
            userName = _userName;
            text = _text;
        }
        public static Message GerErrorCodeObject(int _statusCode)
        {
            return new Message(_statusCode, userName, "Ошибка");
        }
        public override string ToString()
        {
            return statusCode switch
            {
                500 => $"Ошибка входа пользователя. Имя не должно быть пустым",
                501 => $"{userName}: Ошибка, сообщение не должно быть пустым",
                502 => $"Ошибка десериализации либо подключение было прервано",
                503 => $"{userName}: Выход из чата",
                _ => $"{userName}: {text}"
            };
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("StatusCode", statusCode, typeof(int));
            info.AddValue("UserName", userName, typeof(string));
            info.AddValue("Text", text, typeof(string));
        }

        private Message(SerializationInfo info, StreamingContext context)
        {
            statusCode = (int)info.GetValue("StatusCode", typeof(int));
            userName = (string)info.GetValue("UserName", typeof(string));
            text = (string)info.GetValue("Text", typeof(string));

            if (string.IsNullOrEmpty(userName))
            {
                statusCode = 500;
            }                
            else
            {
                if (string.IsNullOrEmpty(text))
                {
                    statusCode = 501;
                }                    
            }
        }
    }
}
