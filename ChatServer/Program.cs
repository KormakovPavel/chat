﻿using System;
using System.Threading;

namespace ChatServer
{
    class Program
    {
        static void Main()
        {
            ServerObject server = new ServerObject();
            Thread listenThread = new Thread(new ThreadStart(server.Listen));
            listenThread.Start();
        }
    }
}
