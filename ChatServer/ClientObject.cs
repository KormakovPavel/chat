﻿using System;
using System.Net.Sockets;
using ChatLib;

namespace ChatServer
{
    class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        readonly TcpClient client;
        readonly ServerObject server;

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            Stream = client.GetStream();
            var message = StreamSerialize.Deserialize(Stream);
            SendMessage(message);
            if (message.StatusCode == 500)
            {
                server.RemoveConnection(Id);
                Close();
            }
            else
            {
                while (true)
                {
                    message = StreamSerialize.Deserialize(Stream);
                    SendMessage(message);
                    if (message.StatusCode == 503)
                    {
                        break;
                    }                        
                }
            }
        }

        private void SendMessage(Message message)
        {
            Console.WriteLine(message.ToString());
            server.BroadcastMessage(message, Id);
        }

        protected internal void Close()
        {
            if (Stream != null)
            {
                Stream.Close();
            }                
            if (client != null)
            {
                client.Close();
            }                
        }
    }
}
