﻿using System;
using System.Net.Sockets;
using ChatLib;

namespace ChatClient
{
    class ClientObject
    {
        private readonly NetworkStream stream;
        private readonly string UserName;
        public ClientObject(string host, int port, string userName)
        {
            UserName = userName;
            TcpClient client = new TcpClient();
            client.Connect(host, port);
            stream = client.GetStream();
            StreamSerialize.Serialize(stream, new Message(UserName, "Вход в чат"));
        }

        public void SendMessage(string messageText)
        {
            StreamSerialize.Serialize(stream, new Message(UserName, messageText));
        }
        public void ReceiveMessage()
        {
            while (true)
            {
                var message = StreamSerialize.Deserialize(stream);
                Console.WriteLine(message.ToString());
                if (message.StatusCode == 502)
                {
                    break;
                }                    
            }
        }
    }
}
