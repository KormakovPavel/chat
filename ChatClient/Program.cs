﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using Microsoft.Extensions.Configuration;

namespace ChatClient
{
    class Program
    {
        static void Main()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appSettings.json");            
            var config = builder.Build();
            var host = config["host"];
            var port = int.Parse(config["port"]);            

            Console.Write("Введите свое имя: ");
            string userName = Console.ReadLine();
            Console.WriteLine($"Добро пожаловать, {userName}");

            ClientObject clientObject = new ClientObject(host, port, userName);
            Thread receiveThread = new Thread(new ThreadStart(clientObject.ReceiveMessage));
            receiveThread.Start();

            Console.WriteLine("Введите сообщение:");
            while (true)
            {
                clientObject.SendMessage(Console.ReadLine());
            }
                
        }
    }
}
